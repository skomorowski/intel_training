﻿using System.Drawing;

namespace GameFoundation
{
	public interface IGame
	{
		string Name { get; }
		int Width { get; }
		int Height { get; }
		IKeyboardStatus KeyboardStatus { get; }

		bool UsePerformanceOptimization { get; }
		void PerformNextIteration(Graphics g, double deltaTime);
	}
}