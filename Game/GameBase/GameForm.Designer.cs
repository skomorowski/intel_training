﻿namespace GameFoundation
{
	partial class GameForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.gameControl1 = new GameFoundation.GameControl();
			this.SuspendLayout();
			// 
			// gameControl1
			// 
			this.gameControl1.Dirty = false;
			this.gameControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gameControl1.Location = new System.Drawing.Point(0, 0);
			this.gameControl1.Name = "gameControl1";
			this.gameControl1.Size = new System.Drawing.Size(438, 252);
			this.gameControl1.TabIndex = 0;
			this.gameControl1.Text = "gameControl1";
			// 
			// GameForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.ClientSize = new System.Drawing.Size(438, 252);
			this.Controls.Add(this.gameControl1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "GameForm";
			this.Text = "GameForm";
			this.Shown += new System.EventHandler(this.GameFormShown);
			this.ResumeLayout(false);

		}

		#endregion

		private GameControl gameControl1;
	}
}