using System;
using System.Drawing;
using System.Threading;

namespace GameFoundation
{
	public class GameControl : DoubleBufferedControl
	{
		private readonly Thread _timeThread;
		private readonly HighPerformanceTimer _timer;
		public Action<Graphics, double> Render;
		private double _deltaTime;

		public GameControl()
		{
			_timer = new HighPerformanceTimer();
			_timer.Start();
			_timeThread = new Thread(Target);
			_timeThread.Start(new TimeThreadData
			{
				TickAction = () => Dirty = true,
				Interval = 1/60.0f,
				SynchronizationContext = SynchronizationContext.Current,
				UsePerformanceOptimization = UsePerformanceOptimization
			});
		}

		public bool UsePerformanceOptimization { get; set; }

		private void Target(object o)
		{
			var data = (TimeThreadData) o;
			var timer = new HighPerformanceTimer();
			try
			{
				timer.Start();
				while (true)
				{
					timer.Stop();
					if (timer.Duration > data.Interval)
					{
						_deltaTime = timer.Duration;
						data.SynchronizationContext.Post(obj => data.TickAction(), null);
						timer.Reset();
					}
					else
					{
						if (data.UsePerformanceOptimization == false)
							continue;

						double delta = data.Interval - timer.Duration;
						if (delta > data.Interval/2)
							Thread.Sleep((int) Math.Floor(1000*(data.Interval - timer.Duration)/2));
						else if (delta > data.Interval/4)
							Thread.Sleep(0);
					}
				}
			}
			catch (ThreadAbortException)
			{
				//end
			}
		}

		protected override void DoDraw(Graphics graphics)
		{
			if (Render == null)
				return;

			_timer.Stop();
			Render(graphics, _deltaTime);
			_timer.Reset();
			_timer.Start();
		}

		protected override void Dispose(bool disposing)
		{
			_timeThread.Abort();
			base.Dispose(disposing);
		}

		private class TimeThreadData
		{
			public double Interval;
			public SynchronizationContext SynchronizationContext;
			public Action TickAction;
			public bool UsePerformanceOptimization;
		}
	}
}