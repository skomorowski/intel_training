﻿using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using GameFoundation;

namespace IntelGame
{
	internal class Game : GameBase
	{
		public Game()
			: base(800, 600)
		{
		}

		public override string Name
		{
			get { return "Gra intelowa"; }
		}

		readonly Font _font = new Font(FontFamily.GenericSerif, 20f);

		public override void PerformNextIteration(Graphics g, double deltaTime)
		{
			g.Clear(Color.Blue);
			g.FillEllipse(Brushes.Yellow, new Rectangle(100, 100, 100, 100));
			g.DrawString(deltaTime.ToString(), _font, Brushes.Yellow, 0f, 0f);
		}
	}
}