namespace SomeGame.Infrastructure
{
	internal interface IVelocityChangeable : IMovingStageObject
	{
		void ChangeVelocity(double dvx, double dvy);
	}
}