using SomeGame.Infrastructure;
using SomeGame.Scenes;

namespace SomeGame.GameObjects
{
	internal abstract class MovingSceneObject : SceneObject, IVelocityChangeable
	{
		protected MovingSceneObject(double posX, double posY, double velocityX, double velocityY)
			: base(posX, posY)
		{
			VelocityX = velocityX;
			VelocityY = velocityY;
		}

		public double VelocityX { get; protected set; }
		public double VelocityY { get; protected set; }

		public void ChangeVelocity(double dvx, double dvy)
		{
			VelocityX += dvx;
			VelocityY += dvy;
		}

		public override void Update(double deltaTime)
		{
			//v=s/t
			//dv=ds/dt
			//ds = dv*dt
			double dsx = VelocityX*deltaTime;
			double dsy = VelocityY*deltaTime;
			PositionX += dsx;
			PositionY += dsy;
		}
	}
}