using System.Drawing;

namespace SomeGame.GameObjects
{
	internal class Square : MovingSceneObject
	{
		public Square(double posX, double posY, double velocityX, double velocityY)
			: base(posX, posY, velocityX, velocityY)
		{
		}

		public override void Draw(Graphics g)
		{
			g.FillRectangle(Brushes.DeepPink,
				(int) (PositionX - 5), (int) (PositionY - 5),
				10, 10);
		}
	}
}