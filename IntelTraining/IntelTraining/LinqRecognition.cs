﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace IntelTraining
{
	[TestFixture]
	public class LinqRecognition
	{
		[Test]
		public void GenerationWorks()
		{
			var ret = GenerateSomeClasses().Take(3);
			Assert.AreEqual(3, ret.Count());
		}

		[Test]
		public void GenerationReturnsDifferentInstances()
		{
			var ret1 = GenerateSomeClasses().Take(1).Single();
			var ret2 = GenerateSomeClasses().Take(1).Single();

			Assert.AreNotSame(ret1, ret2);
		}

		[Test]
		public void GenerationExpressionReturnsDifferentInstances()
		{
			var generatingExpression = GenerateSomeClasses().Take(1);

			var ret1 = generatingExpression.Single();
			var ret2 = generatingExpression.Single();

			Assert.AreNotSame(ret1, ret2);
		}

		[Test]
		public void GenerationExpressionMaterializedReturnsSameInstances()
		{
			var generatingExpression = GenerateSomeClasses().Take(1).ToList();

			var ret1 = generatingExpression.Single();
			var ret2 = generatingExpression.Single();

			Assert.AreSame(ret1, ret2);
		}

		class SomeClass { }

		static IEnumerable<SomeClass> GenerateSomeClasses()
		{
			while(true)
				yield return new SomeClass();
		}
	}
}