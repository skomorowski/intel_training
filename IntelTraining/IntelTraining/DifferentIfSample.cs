using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace IntelTraining
{
	[TestFixture]
	public class DifferentIfSample
	{
		[Test]
		public void CalculateSomethingUponOptionsSimple()
		{
			var ret1 = CalculateSomethingUponOptions(ESomeOptions.Option1);
			var ret2 = CalculateSomethingUponOptions(ESomeOptions.Option2);
			var ret3 = CalculateSomethingUponOptions(ESomeOptions.Option3);

			Assert.AreEqual(1, ret1);
			Assert.AreEqual(2, ret2);
			Assert.AreEqual(3, ret3);

			Assert.Throws<ArgumentOutOfRangeException>(
				() => CalculateSomethingUponOptions(ESomeOptions.Option4));
		}

		[Test]
		public void CalculateSomethingUponOptionsWithPatterns()
		{
			var interpreter = CreateInterpreter();

			var ret1 = interpreter.CalculateSomethingUponOptions(ESomeOptions.Option1);
			var ret2 = interpreter.CalculateSomethingUponOptions(ESomeOptions.Option2);
			var ret3 = interpreter.CalculateSomethingUponOptions(ESomeOptions.Option3);

			Assert.AreEqual(1, ret1);
			Assert.AreEqual(2, ret2);
			Assert.AreEqual(3, ret3);

			Assert.Throws<ArgumentOutOfRangeException>(
				() => interpreter.CalculateSomethingUponOptions(ESomeOptions.Option4));
		}

		ICalculationInterpreter CreateInterpreter()
		{
			return new CalculationInterpreter();
		}


		#region helpers

		enum ESomeOptions
		{
			Option1,
			Option2,
			Option3,
			Option4,
		}

		int CalculateSomethingUponOptions(ESomeOptions option)
		{
			switch (option)
			{
				case ESomeOptions.Option1:
					return 1;
				case ESomeOptions.Option2:
					return 2;
				case ESomeOptions.Option3:
					return 3;
				default:
					throw new ArgumentOutOfRangeException("option");
			}
		}

		interface ISomethingCalculationStrategy
		{
			ESomeOptions SuitableForOption { get; }
			int Calculate();
		}

		private class SomethingCalculationStrategy1 : ISomethingCalculationStrategy
		{
			public ESomeOptions SuitableForOption { get { return ESomeOptions.Option1; } }
			public int Calculate()
			{
				return 1;
			}
		}
		private class SomethingCalculationStrategy2 : ISomethingCalculationStrategy
		{
			public ESomeOptions SuitableForOption { get { return ESomeOptions.Option2; } }
			public int Calculate()
			{
				return 2;
			}
		}
		private class SomethingCalculationStrategy3 : ISomethingCalculationStrategy
		{
			public ESomeOptions SuitableForOption { get { return ESomeOptions.Option3; } }
			public int Calculate()
			{
				return 3;
			}
		}

		interface ICalculationInterpreter
		{
			int CalculateSomethingUponOptions(ESomeOptions option);
		}

		class CalculationInterpreter : ICalculationInterpreter
		{
			private readonly IEnumerable<ISomethingCalculationStrategy> _calculationStrategies;

			public CalculationInterpreter()
			{
				_calculationStrategies = new List<ISomethingCalculationStrategy>()
				{
					new SomethingCalculationStrategy1(),
					new SomethingCalculationStrategy2(),
					new SomethingCalculationStrategy3(),
				};
			}

			public int CalculateSomethingUponOptions(ESomeOptions option)
			{
				var strategy = _calculationStrategies
					.SingleOrDefault(e => e.SuitableForOption == option);

				if (strategy == null)
					throw new ArgumentOutOfRangeException();

				return strategy.Calculate();
			}
		}

		#endregion
	}
}