﻿namespace IntelTraining.ValidationCase.Infrastructure
{
	public interface IBackendService
	{
		OutputData DoSomething(InputData data);
	}
}