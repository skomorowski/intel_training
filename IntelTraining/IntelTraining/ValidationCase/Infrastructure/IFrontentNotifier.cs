﻿namespace IntelTraining.ValidationCase.Infrastructure
{
	public interface IFrontentNotifier
	{
		void Notify(string msg);
	}
}