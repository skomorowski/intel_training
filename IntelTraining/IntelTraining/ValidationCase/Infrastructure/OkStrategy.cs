﻿namespace IntelTraining.ValidationCase.Infrastructure
{
	class OkStrategy : IBackendServiceDoSomethingResultDealingStrategy
	{
		private readonly IFrontentNotifier _frontentNotifier;

		public OkStrategy(IFrontentNotifier frontentNotifier)
		{
			_frontentNotifier = frontentNotifier;
		}

		public OutputType SuitableFor { get { return OutputType.Ok; } }
		public void Deal(OutputData data)
		{
			_frontentNotifier.Notify("ok: " + data.RetVal);
		}
	}
}