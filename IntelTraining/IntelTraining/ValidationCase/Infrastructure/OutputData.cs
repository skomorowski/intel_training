﻿namespace IntelTraining.ValidationCase.Infrastructure
{
	public class OutputData
	{
		public OutputType Type { get; set; }
		public string RetVal { get; set; }
	}
}