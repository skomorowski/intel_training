﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.Unity;

namespace IntelTraining.ValidationCase.Infrastructure
{
	class BackendServiceFacade
	{
		private readonly IBackendService _svc;
		private readonly IEnumerable<IBackendServiceDoSomethingResultDealingStrategy> _strategies;

		public BackendServiceFacade(IUnityContainer unityContainer, IBackendService svc)
		{
			_svc = svc;
			_strategies = unityContainer.ResolveAll<IBackendServiceDoSomethingResultDealingStrategy>();
		}

		public void InvokeBackendService(InputData data)
		{
			var ret = new BackendServiceDoSomethingRetryDecorator(_svc).DoSomething(data);
			Deal(ret);
		}

		public void Deal(OutputData data)
		{
			var ret = _strategies.Single(e => e.SuitableFor == data.Type);
			ret.Deal(data);
		}
	}
}