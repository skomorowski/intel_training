﻿namespace IntelTraining.ValidationCase.Infrastructure
{
	class BusyStrategy : IBackendServiceDoSomethingResultDealingStrategy
	{
		private readonly IFrontentNotifier _frontentNotifier;

		public BusyStrategy(IFrontentNotifier frontentNotifier)
		{
			_frontentNotifier = frontentNotifier;
		}

		public OutputType SuitableFor { get { return OutputType.Busy; } }
		public void Deal(OutputData data)
		{
			_frontentNotifier.Notify("busy: " + data.RetVal);
		}
	}
}