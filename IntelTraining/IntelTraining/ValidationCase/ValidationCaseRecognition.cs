﻿using System.CodeDom;
using IntelTraining.ValidationCase.Infrastructure;
using Microsoft.Practices.Unity;
using NSubstitute;
using NUnit.Framework;

namespace IntelTraining.ValidationCase
{
	[TestFixture]
	public class ValidationCaseRecognition
	{
		[Test]
		public void backend_service_do_something_retry_decorator_works()
		{
			var ibs = Substitute.For<IBackendService>();
			ibs.DoSomething(Arg.Any<InputData>())
				.Returns(e => new OutputData() { Type = OutputType.Busy }, e => new OutputData() { Type = OutputType.Ok });

			var dec = new BackendServiceDoSomethingRetryDecorator(ibs);
			dec.DoSomething(new InputData());
			Assert.AreEqual(1, dec.LeftRetries);
		}

		[Test]
		public void strategies_notify_good_messages()
		{
			var ibs = Substitute.For<IBackendService>();
			ibs.DoSomething(Arg.Any<InputData>())
				.Returns(e => new OutputData() { Type = OutputType.Busy }, e => new OutputData() { Type = OutputType.Ok, RetVal = "val"});

			string msg = null;
			var notifierMock = Substitute.For<IFrontentNotifier>();
			notifierMock.When(e => e.Notify(Arg.Any<string>()))
				.Do(e => msg = e.Arg<string>());

			var container = new UnityContainer();
			container.RegisterType<IBackendServiceDoSomethingResultDealingStrategy, OkStrategy>("OkStrategy")
				.RegisterType<IBackendServiceDoSomethingResultDealingStrategy, BusyStrategy>("BusyStrategy")
				.RegisterType<IBackendServiceDoSomethingResultDealingStrategy, ErrorStrategy>("ErrorStrategy")
				.RegisterInstance(notifierMock)
				.RegisterInstance(ibs);

			var facade = container.Resolve<BackendServiceFacade>();
			facade.Deal(new OutputData() { Type = OutputType.Ok, RetVal = "val" });

			Assert.AreEqual("ok: val", msg);

			msg = "";
			facade.InvokeBackendService(new InputData());
			Assert.AreEqual("ok: val", msg);
		}

		[Test]
		public void okstrategy_works_fine()
		{
			string msg = null;
			var notifierMock = Substitute.For<IFrontentNotifier>();
			notifierMock.When(e => e.Notify(Arg.Any<string>()))
				.Do(e => msg = e.Arg<string>());

			var strategy = new OkStrategy(notifierMock);
			strategy.Deal(new OutputData() { Type = OutputType.Ok, RetVal = "val" });

			Assert.AreEqual("ok: val", msg);
		}
	}
}