﻿using NUnit.Framework;

namespace IntelTraining.DesignPatterns
{
	[TestFixture]
	public class MementoSample
	{
		[Test]
		public void memento_works()
		{
			var myClass = new SomeClass() { State = 3 };

			IMemento memento = myClass;
			var savedState = memento.GetMemento();

			myClass.State = 7;

			memento.SetMemento(savedState);

			Assert.AreEqual(3, myClass.State);
		}

		class SomeClass : IMemento
		{
			public int State { get; set; }
			public IMementoData GetMemento()
			{
				return new SomeClassMemento() { SavedState = State };
			}

			public void SetMemento(IMementoData memento)
			{
				var myMemento = (SomeClassMemento)memento;
				State = myMemento.SavedState;
			}
		}

		interface IMemento
		{
			IMementoData GetMemento();
			void SetMemento(IMementoData memento);
		}
		interface IMementoData { }
		class SomeClassMemento : IMementoData
		{
			public int SavedState { get; set; }
		}
	}
}