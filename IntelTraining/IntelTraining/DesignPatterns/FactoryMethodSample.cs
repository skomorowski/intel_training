﻿using System;
using NUnit.Framework;

namespace IntelTraining.DesignPatterns
{
	[TestFixture]
	public class FactoryMethodSample
	{
		[Test]
		public void factory_method()
		{
			var ismth = SomeFactory.CreateSomething();
			Assert.Throws<NotImplementedException>(ismth.DoSomething);
		}

		static class SomeFactory
		{
			public static ISomeStrategy CreateSomething()
			{
				//if(SomeGlobalSettings.ShouldCreate)
				return new SomeStrategy();
			}

			public static ISomeStrategy CreateSomething(int val)
			{
				//if(val==1)
				return new SomeStrategy();
			}
		}

		interface ISomeStrategy
		{
			void DoSomething();
		}

		private class SomeStrategy : ISomeStrategy
		{
			public void DoSomething()
			{
				throw new System.NotImplementedException();
			}
		}
	}
}