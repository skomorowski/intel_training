﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace IntelTraining
{
	public static class SerializationHelper
	{
		public static string ToString(object obj)
		{
			try
			{
				var serializer = new XmlSerializer(obj.GetType());
				using (var writer = new StringWriter())
				{
					serializer.Serialize(writer, obj);
					return writer.ToString();
				}
			}
			catch (Exception)
			{
				return string.Empty;
			}
		}

		public static T FromString<T>(string xml)
		{
			if (xml == null)
				return default(T);

			var serializer = new XmlSerializer(typeof(T));
			using (var reader = new StringReader(xml))
			{
				try
				{
					return (T)serializer.Deserialize(reader);
				}
				catch
				{
					return default(T);
				}
			}
		}
	}
}