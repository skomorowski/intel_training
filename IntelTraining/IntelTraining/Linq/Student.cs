namespace IntelTraining.Linq
{
	class Student
	{
		public string Name { get; set; }
		public string Surname { get; set; }
		public decimal Avarage { get; set; }
		public string IndexNo { get; set; }
		public decimal TotalPayments { get; set; }
	}
}