using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace IntelTraining
{
	[TestFixture]
	public class CleanCodeSample
	{
		[Test]
		public void function_xhfg_works()
		{
			var tab = new[] { 4, 5, 3, 2, 1 };
			xhfg(tab);
			Assert.AreEqual(1, tab[0]);
			Assert.AreEqual(2, tab[1]);
			Assert.AreEqual(3, tab[2]);
			Assert.AreEqual(4, tab[3]);
			Assert.AreEqual(5, tab[4]);
		}

		[Test]
		public void refactored_sort_works()
		{
			var tab = new[] { 4, 5, 3, 2, 1 };

			var sort = new SomeSort(tab);
			var sortedData = sort.GenerateSortedData().ToList();

			Assert.AreEqual(1, sortedData[0]);
			Assert.AreEqual(2, sortedData[1]);
			Assert.AreEqual(3, sortedData[2]);
			Assert.AreEqual(4, sortedData[3]);
			Assert.AreEqual(5, sortedData[4]);
		}

		#region helpers
		static void xhfg(int[] tab)
		{
			for (int i = 0; i < tab.Length; ++i)
			{
				for (int j = i; j > 0; --j)
				{
					if (tab[j - 1] > tab[j])
					{
						int t = tab[j - 1];
						tab[j - 1] = tab[j];
						tab[j] = t;
					}
					else
						break;
				}
			}
		}

		class SomeSort
		{
			private readonly IEnumerable<int> _toSort;

			public SomeSort(IEnumerable<int> toSort)
			{
				_toSort = toSort.ToList();
			}

			public IEnumerable<int> GenerateSortedData()
			{
				var tab = _toSort.ToArray();
				IterateFromBeginningToEnd(tab);
				return tab;
			}

			static void IterateFromBeginningToEnd(int[] tab)
			{
				for (int i = 1; i < tab.Length; ++i)
				{
					IterateFromCurrentPositionToBeginning(tab, i);
				}
			}

			static void IterateFromCurrentPositionToBeginning(int[] tab, int pos)
			{
				for (int j = pos; j > 0; --j)
				{
					if (IsPreviousElementBiggerThanCurrentOne(tab, j))
					{
						SwapCurrentElementWithPreviousOne(tab, j);
					}
					else
						break;
				}
			}

			static bool IsPreviousElementBiggerThanCurrentOne(int[] tab, int pos)
			{
				return tab[pos - 1] > tab[pos];
			}

			static void SwapCurrentElementWithPreviousOne(int[] tab, int pos)
			{
				DoSwap(ref tab[pos - 1], ref tab[pos]);
			}

			static void DoSwap(ref int a, ref int b)
			{
				int t = a;
				a = b;
				b = t;
			}
		}
		#endregion

	}
}