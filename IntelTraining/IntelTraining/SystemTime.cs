﻿using System;

namespace IntelTraining
{
	public static class SystemTime
	{
		private static readonly Func<DateTime> DefaultLogic = () => DateTime.UtcNow;

		private static Func<DateTime> _current = DefaultLogic;

		public static DateTime Current
		{
			get { return _current(); }
		}

		public static void _replaceCurrentTimeLogic(Func<DateTime> newCurrentTimeLogic)
		{
			_current = newCurrentTimeLogic;
		}

		public static void _revertToDefaultLogic()
		{
			_current = DefaultLogic;
		}
	}
}