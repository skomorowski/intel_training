﻿using System.IO;
using System.Net;
using System.Text;

namespace IntelTraining.ExternalProcessorCase.ExternalService
{
	public static class ExternalService
	{
		public static string GetUrl(string url)
		{
			var request = (HttpWebRequest)WebRequest.Create(url);

			request.Method = "GET";
			var response = request.GetResponse();

			var dataStream = response.GetResponseStream();

			var ms = new MemoryStream();

			int count;
			var size = (int)response.ContentLength;
			var buf = new byte[size];
			do
			{
				count = dataStream.Read(buf, 0, size);
				ms.Write(buf, 0, count);
			} while (dataStream.CanRead && count > 0);

			var bytes = ms.ToArray();
			dataStream.Close();
			response.Close();
			ms.Close();
			return ASCIIEncoding.ASCII.GetString(bytes);
		}
	}
}