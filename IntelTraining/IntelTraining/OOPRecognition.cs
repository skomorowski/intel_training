﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace IntelTraining
{
	[TestFixture]
	public class OOPRecognition
	{
		[Test]
		public void HermetizationSample()
		{
			var appender = new StringAppender(100);

			appender.Append("aaa");
			appender.Append("bbbb");

			Assert.AreEqual(7, appender.Length);
			Assert.AreEqual("aaabbbb", appender.AllText);

			var app2 = new StringAppender(4);
			app2.Append("aabbb");
			Assert.AreEqual(4, app2.Length);
			Assert.AreEqual("abbb", app2.AllText);

			app2.Append("cc");
			Assert.AreEqual(4, app2.Length);
			Assert.AreEqual("bbcc", app2.AllText);
		}

		[Test]
		public void InheritanceSample()
		{
			var s1 = new SomeBaseClass();
			Assert.IsInstanceOf<SomeBaseClass>(s1);
			Assert.IsNotInstanceOf<SomeDereivedClass>(s1);

			var s2 = new SomeDereivedClass();
			Assert.IsInstanceOf<SomeBaseClass>(s2);
			Assert.IsInstanceOf<SomeDereivedClass>(s2);
		}

		[Test]
		public void PolymorphismSample()
		{
			var s1 = new PolyBase();
			Assert.AreEqual(5, s1.GenerateValue());

			var s2 = new PolyDereived();
			Assert.AreEqual(10, s2.GenerateValue());

			PolyBase s3 = new PolyDereived();
			Assert.AreEqual(10, s3.GenerateValue());

			var lst = new List<PolyBase>()
			{
				new PolyBase(),
				new PolyDereived(),
			};
			var vals = lst.Select(e => e.GenerateValue()).ToList();
			var val1 = vals[0];
			var val2 = vals[1];
			Assert.AreEqual(5, val1);
			Assert.AreEqual(10, val2);
		}

		[Test]
		public void TemplateMethod()
		{
			var sga = new OrderedSequence(5);
			Assert.AreEqual("1 2 3 4 5", sga.NumbersAsText());

			var sga2 = new OrderedMultipliedBy2Sequence(5);
			Assert.AreEqual("2 4 6 8 10", sga2.NumbersAsText());

			var lst = new List<SequenceGenerationAlgorithm>()
			{
				new OrderedSequence(3),
				new OrderedMultipliedBy2Sequence(4)
			};
			var vals = lst.Select(e => e.NumbersAsText()).ToList();
			var v1 = vals[0];
			var v2 = vals[1];
			Assert.AreEqual("1 2 3", v1);
			Assert.AreEqual("2 4 6 8", v2);
		}

		#region helpers
		class StringAppender
		{
			private readonly int _maxLength;

			public StringAppender(int maxLength)
			{
				_maxLength = maxLength;
			}

			private string _str = string.Empty;
			private int _length;

			public void Append(string str)
			{
				_str += str;
				_length += str.Length;

				if (Length > _maxLength)
				{
					_str = _str.Substring(_length - _maxLength);
					_length = _maxLength;
				}
			}

			public int Length { get { return _length; } }

			public string AllText { get { return _str; } }
		}

		class SomeBaseClass { }
		class SomeDereivedClass : SomeBaseClass { }

		class PolyBase
		{
			public virtual int GenerateValue()
			{
				return 5;
			}
		}

		class PolyDereived : PolyBase
		{
			public override int GenerateValue()
			{
				return 10;
			}
		}

		abstract class SequenceGenerationAlgorithm
		{
			protected readonly int Length;

			protected SequenceGenerationAlgorithm(int length)
			{
				Length = length;
			}

			public string NumbersAsText()
			{
				var numbers = Generate();
				return string.Join(" ", numbers);
			}

			protected abstract IEnumerable<int> Generate();
		}

		class OrderedSequence : SequenceGenerationAlgorithm
		{
			public OrderedSequence(int length)
				: base(length)
			{
			}

			protected override IEnumerable<int> Generate()
			{
				return GennerateInner().Take(Length).ToList();
			}

			IEnumerable<int> GennerateInner()
			{
				int i = 1;
				while (true)
				{
					yield return i;
					++i;
				}
			}
		}

		class OrderedMultipliedBy2Sequence : SequenceGenerationAlgorithm
		{
			public OrderedMultipliedBy2Sequence(int length)
				: base(length)
			{
			}

			protected override IEnumerable<int> Generate()
			{
				return GennerateInner().Take(Length).ToList();
			}

			IEnumerable<int> GennerateInner()
			{
				int i = 1;
				while (true)
				{
					yield return i*2;
					++i;
				}
			}
		}

		#endregion
	}
}